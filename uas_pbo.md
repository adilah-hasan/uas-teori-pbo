# No 1
USE CASE

Use Case dari User

![Use Case User](https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/use_case_user.png)

Use Case dari Sistem

![Use Case Sistem](https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/use_case_sistem.png)

Use Case dari direksi

![Use Case direksi](https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/use_case_direksi.png)


# No 2

berikut adalah diagram class dari projek web service yang dibuat

![Class Diagram](https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/class_diagram_uas.drawio.png)

jadi pada class diagram diatas menjelaskan bahwa projek yang dibuat hanya berupa menu register dan login dan membuat profil.
yang dihubungkan dengan suatu database.

# No 3

SOLID adalah singkatan dari lima prinsip desain perangkat lunak yang bertujuan untuk membuat desain yang baik, kuat, dan mudah dikelola. Prinsip-prinsip ini membantu menghasilkan kode yang lebih mudah dipahami, fleksibel, dan dapat diperluas. Berikut adalah penjelasan singkat untuk masing-masing prinsip SOLID:

1. Single Responsibility Principle (SRP):
   Prinsip ini menyatakan bahwa setiap kelas atau modul seharusnya hanya memiliki satu tanggung jawab tunggal. Sebuah kelas atau modul harus bertanggung jawab terhadap satu aspek atau fungsi tertentu dalam sistem. Dengan memisahkan tanggung jawab, perubahan atau pemeliharaan dapat dilakukan dengan lebih mudah dan tidak mempengaruhi komponen lain.
[https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/config.py]
   contoh pada kelas config.py hanya bertanggungjawab atas class config saja, walaupun didalamnya terdapat deklarasi fungsi-fungsi baru.

2. Open-Closed Principle (OCP):
   Prinsip ini menyatakan bahwa entitas perangkat lunak (kelas, modul, dll.) harus terbuka untuk perluasan (open for extension) tetapi tertutup untuk modifikasi (closed for modification). Artinya, kita sebaiknya menghindari memodifikasi kode yang sudah ada ketika ada perubahan, dan lebih baik melakukan perluasan dengan menambahkan fitur baru.

3. Liskov Substitution Principle (LSP):
   Prinsip ini menyatakan bahwa objek dari kelas turunan harus bisa digunakan sebagai pengganti objek kelas induk tanpa mengganggu kebenaran program. Dengan kata lain, kelas turunan harus mematuhi kontrak yang didefinisikan oleh kelas induk dan tidak mengubah perilaku yang diharapkan dari kelas tersebut.

4. Interface Segregation Principle (ISP):
   Prinsip ini menyatakan bahwa antarmuka yang dikembangkan untuk klien seharusnya spesifik dan tidak memaksa klien untuk mengimplementasikan metode yang tidak mereka perlukan. Tujuan prinsip ini adalah menghindari antarmuka yang gemuk dan memisahkan antarmuka menjadi bagian-bagian yang lebih kecil dan spesifik.

5. Dependency Inversion Principle (DIP):
   Prinsip ini menyatakan bahwa kelas-kelas tingkat tinggi tidak seharusnya bergantung pada kelas tingkat rendah secara langsung, tetapi pada abstraksi. Ini menggambarkan pentingnya bergantung pada antarmuka atau kontrak yang lebih abstrak daripada bergantung pada implementasi konkrit. Dengan prinsip ini, kita dapat mencapai fleksibilitas dan penggantian komponen yang lebih mudah.


# No 4
Desain Pattern yang dipilih : 
Pola Desain MVC (Model-View-Controller):
Pola MVC digunakan untuk memisahkan antara data (model), tampilan (view), dan logika aplikasi (controller). Model mewakili data dan perilaku, view bertanggung jawab untuk menampilkan data, dan controller mengatur interaksi antara model dan view. Pola ini membantu dalam pengorganisasian kode dan pemisahan tanggung jawab.

pada projek yang saya buat menggunakan konsep pola desain MVC, yaitu dengan memisahkan model program/case, tampilannya, dan controllernya. memang di projek yang saya belum sempurna menerapkan itu, akan tetapi pada konsep nya saya menerapkan pola tersebut. dapat dilihat file koding saya dibawah (link),disana merupakan contoh model dari kelas user, adapun controllernya dan tampilannya. akan tetapi perlu diingat bahwa ini belum selesai sempurna.

[https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/base_response.py]
[https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/config.py]
[https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/user.py]

# No 5

dari kode ini [https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/get_db_session.py], maka dapat di simpulkan
Fungsi `get_db_session` digunakan untuk mendapatkan sesi database yang terhubung ke basis data yang ditentukan oleh objek `db_engine`. Dalam implementasinya, fungsi ini melakukan inisialisasi sesi menggunakan objek `db_engine`, dan menggunakan `yield` untuk menghasilkan sesi yang dapat digunakan. Jika terjadi kesalahan, fungsi akan melakukan rollback pada sesi dan mengangkat kembali pengecualian. Setelah itu, sesi ditutup setelah penggunaan. Dengan demikian, fungsi ini menyediakan konektivitas dan pengelolaan sesi yang dapat digunakan untuk operasi database dalam aplikasi.



# No 6


1. Inisialisasi
dengan menginstall FastAPI dan library yang dibutuhkan
2. Import Modul dan Inisialisasi Aplikasi:
   ```python
   from fastapi import FastAPI
   from pydantic import BaseModel

   app = FastAPI()
   ```

3. Definisikan Model Data:
   ```python
   class Item(BaseModel):
       id: int
       name: str
       price: float
   ```

4. Simpan Data:
   ```python
   database = []

   @app.post("/items/")
   def create_item(item: Item):
       database.append(item)
       return {"message": "Item created successfully"}
   ```

5. Baca Data:
   ```python
   @app.get("/items/{item_id}")
   def read_item(item_id: int):
       for item in database:
           if item.id == item_id:
               return item
       return {"message": "Item not found"}
   ```

6. Update Data:
   ```python
   @app.put("/items/{item_id}")
   def update_item(item_id: int, item: Item):
       for i in range(len(database)):
           if database[i].id == item_id:
               database[i] = item
               return {"message": "Item updated successfully"}
       return {"message": "Item not found"}
   ```

7. Hapus Data:
   ```python
   @app.delete("/items/{item_id}")
   def delete_item(item_id: int):
       for i in range(len(database)):
           if database[i].id == item_id:
               del database[i]
               return {"message": "Item deleted successfully"}
       return {"message": "Item not found"}
   ```

8. Jalankan Aplikasi:
   ```python
   if __name__ == "__main__":
       import uvicorn
       uvicorn.run(app, host="0.0.0.0", port=8000)
   ```



# No 7

[https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/base_response.py]

tampilan pada program ini belum sempurna, masih ditampilkn pada terminal dan di FastAPI nya saja. rencananya ingin membuatnya dengan react.js akan tetapi dengan sedikitnya ilmu, dan pemahaman yang perlu disempurnkan lagi, juga waktu mengulik yang terus2an bertabrakan dengan hal lain, dan bahkan dipangkas beberapa hari membuat saya tidak sempat menyelesaikan tugas saya. 

untuk itu saya memohon maaf yang sebesar-besarnya kepada Pak Insan

# No 8

autentikasi yang saya gunakan dalam menghubungkan http dengan fastAPI adalah ketika refresh token.
jadi yang saya gunakan dalam penghubungan antara http dan fastAPI ini untuk httpbarear token

[https://gitlab.com/adilah-hasan/uas-teori-pbo/-/blob/main/authentication.py]

penjelasan kode nya :

1. Kelas `Authentication` merupakan turunan dari kelas `HTTPBearer`, yang mengindikasikan bahwa kami menggunakan skema autentikasi Bearer token.
2. Metode `__call__` dalam kelas `Authentication` digunakan untuk memvalidasi dan mendapatkan informasi autentikasi dari header Authorization pada objek `Request`.
3. Dalam metode `__call__`, kami menggunakan metode induk `__call__` dari kelas `HTTPBearer` untuk mendapatkan kredensial autentikasi dari header Authorization.
4. Kami kemudian memanggil fungsi `get_payload` untuk mendapatkan payload (isi) dari token Bearer yang diberikan dalam autentikasi.
5. Jika terjadi pengecualian saat memperoleh payload, kami menaikkan pengecualian HTTPException yang sesuai, seperti Token expired (40100) atau Token invalid (40101).
6. Jika autentikasi berhasil dan tidak ada pengecualian, payload autentikasi tersebut dikembalikan.
7. Kelas `Authentication` dapat digunakan sebagai dependency di endpoint FastAPI untuk memastikan bahwa akses endpoint hanya diberikan kepada pengguna yang telah melakukan autentikasi dengan token Bearer yang valid.


# No 9

[https://youtu.be/IIgTmfoM9kM]

# No 10
Demostrasi Machine Learning ChatGPT

```python
from transformers import GPT2LMHeadModel, GPT2Tokenizer

# Menginisialisasi model dan tokenizer GPT
model_name = 'gpt3.5-turbo'
tokenizer = GPT2Tokenizer.from_pretrained(model_name)
model = GPT2LMHeadModel.from_pretrained(model_name)

# Fungsi untuk menghasilkan respons dari model GPT-3.5
def generate_response(user_input):
    # Mengonversi input pengguna menjadi token yang sesuai
    input_ids = tokenizer.encode(user_input, return_tensors='pt')

    # Menggunakan model untuk menghasilkan respons
    response = model.generate(input_ids, max_length=100, num_return_sequences=1)

    # Mendekodekan respons yang dihasilkan menjadi teks yang dapat dibaca
    response_text = tokenizer.decode(response[:, input_ids.shape[-1]:][0], skip_special_tokens=True)
    
    return response_text

# Menggunakan fungsi generate_response untuk berinteraksi dengan pengguna
while True:
    user_input = input("User: ")
    response = generate_response(user_input)
    print("ChatGPT: " + response)
```

Dalam contoh di atas, kami menggunakan library Transformers dari Hugging Face untuk menggunakan model GPT-3.5 Turbo. Tahapan penggunaannya adalah sebagai berikut:

1. Menginisialisasi model dan tokenizer GPT-3.5 Turbo menggunakan `GPT2LMHeadModel` dan `GPT2Tokenizer` dari library Transformers.

2. Membuat fungsi `generate_response` yang mengambil input pengguna, mengonversinya menjadi token menggunakan tokenizer, dan kemudian menggunakan model untuk menghasilkan respons teks.

3. Dalam loop `while True`, kita dapat berinteraksi dengan pengguna melalui input dari terminal. Input pengguna akan dikirim ke fungsi `generate_response`, dan respons yang dihasilkan akan dicetak ke layar sebagai output.

Pastikan Anda telah menginstal library Transformers dan memilih versi model yang sesuai (misalnya, GPT-3.5 Turbo) dari Hugging Face Model Hub sebelum menjalankan contoh di atas.

Ingatlah bahwa ini hanya contoh sederhana, dan implementasi yang lebih kompleks dan disesuaikan dapat dilakukan sesuai dengan kebutuhan proyek yang lebih spesifik.
